/*
1.	Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
Екранування - це використання символу "\" для того, щоб редактор міг сприймати символ, який стоїть після "\" буквально.

2.Які засоби оголошення функцій ви знаєте?
функцію можна оголосити за допомогою наступних засобів:
-function declaration,
-function expression,
-через стрілочну функцію.

3.Що таке hoisting, як він працює для змінних та функцій?
hoisting - це механізм, при якому змінні та оголошення функції піднімаються вгору по своїй області видимості
перед виконанням коду
 */

function createNewUser() {
    let newUser = new Object ();
        newUser.userName = prompt("Enter you name");
        newUser.userSurname = prompt("Enter you Surname");
        newUser.birthday = prompt('Enter your date of birth', 'dd.mm.yyyy');
        newUser.getLogin = function (){
        return `${(newUser.userName + newUser.userSurname).toLowerCase()}`
    };
    newUser.getAge = function () {
        return `${new Date().getFullYear() - newUser.birthday.slice(6)}`;
    };
    newUser.getPassword = function () {
        return `${newUser.userName[0].toUpperCase() + newUser.userSurname.toLowerCase() + newUser.birthday.slice(6)}`;
    };
    return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

